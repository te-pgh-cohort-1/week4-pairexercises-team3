﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using FindAndReplace;
using System.Linq;

namespace FindAndReplace
{
    public class FindAndReplaceClass
    {
        public void SearchMethod()
        {

            Console.WriteLine("Please enter a search phrase: ");
            string userSearchPhrase = Console.ReadLine();

            Console.WriteLine("What would you like to replace the search phrase with?: ");
            string userReplacePhrase = Console.ReadLine();

            Console.WriteLine("Please enter full source file path: ");
            string userSourceFilePath = Console.ReadLine();


            if (!File.Exists(userSourceFilePath))
            {
                Console.WriteLine("Invalid Full File Path");
                SearchMethod();
            }


            Console.WriteLine("Please enter the destination file path: ");
            string userDestinationFilePath = Console.ReadLine();

            Console.WriteLine("What would you like to name the new file?");
            string userNewFileName = Console.ReadLine();


            string newUserFile = Path.Combine(userDestinationFilePath, userNewFileName + ".txt");



            if (File.Exists(newUserFile))
            {
                Console.WriteLine("File already exists in this location.");
            }


            using (StreamReader sr = new StreamReader(userSourceFilePath))
            {
                {
                    using (StreamWriter sw = new StreamWriter(newUserFile, true))

                        while (!sr.EndOfStream)
                        {
                            string eachLine = sr.ReadLine();

                            string replacedLine = eachLine.Replace(userSearchPhrase, userReplacePhrase);

                            sw.WriteLine(replacedLine);
                        }
                }
            }
        }
    }
}
