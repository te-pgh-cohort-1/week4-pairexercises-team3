﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace file_io_part1_exercises_pair
{
    public class GetTextClass
    {
        public void GetText()
        {

            Console.WriteLine("Please enter a file path: ");
            string filePath = Console.ReadLine();




            int numberOfSentences = 0;
            int numberOfWords = 0;


            using (StreamReader sr = new StreamReader(filePath))
            {
                while (!sr.EndOfStream)
                {
                    string[] wordsPerLineArray = sr.ReadLine().Split(" ", StringSplitOptions.RemoveEmptyEntries);
                    numberOfWords += wordsPerLineArray.Length;
                }
            }

            using (StreamReader sr = new StreamReader(filePath))
            {
                while (!sr.EndOfStream)
                {
                    string[] sentencesArray = sr.ReadLine().Split('.', '!', '?');
                    numberOfSentences += sentencesArray.Length;
                }
            }



            Console.WriteLine("Number of words: " + numberOfWords);
            Console.WriteLine("Number of sentences: " + numberOfSentences);
            Console.ReadLine();
        }
    }
}
