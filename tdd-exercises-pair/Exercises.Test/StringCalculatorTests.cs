using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System;
using System.Linq;
using Exercises;

namespace Exercises.Test
{
    [TestClass]
    public class StringCalculatorTests
    {

        StringCalculator testClass = new StringCalculator();  //Global Class Instance

        [TestMethod]
        public void TestEmptyStringInput()
        {
            string testNumber = "";

            int returnedNumber = testClass.Add(testNumber);

            Assert.AreEqual(returnedNumber, 0, "Method should return 0.");
        }

        [TestMethod]
        public void TestSingleStringInput()
        {
            string testNumber = "133";

            int returnedNumber = testClass.Add(testNumber);

            Assert.AreEqual(returnedNumber, 133, "Method should return '133'");
        }

        [TestMethod]
        public void TestTwoStringsInput()
        {
            string testNumber = "1, 2, 5, 8";

            int returnedNumber = testClass.Add(testNumber);

            Assert.AreEqual(returnedNumber, 16, "Method should return '16'");
        }

        [TestMethod]
        public void TestLineBreaksInStringInput()
        {
            string testNumber = "3\n5\n2,4";

            int returnedNumber = testClass.Add(testNumber);

            Assert.AreEqual(returnedNumber, 14, "Method should return '14'");
        }

        //[TestMethod]
        //public void TestMultipleDelimetersInInput()
        //{
        //    string testNumber = "//;\n1;2";

        //    int returnedNumber = testClass.Add(testNumber);

        //    Assert.AreEqual(returnedNumber, 3, "Method should return '3'");
        //}

    }
}
