﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Exercises;

namespace Exercises.Test
{
    [TestClass]
    public class NumbersToWordsTests
    {

        NumbersToWords testClass = new NumbersToWords();        //Global instance of class we're using

        [TestMethod]
        public void TestSingleDigits()
        {
            int num = 2;

            string returnedString = testClass.NumsToWords(num);

            Assert.AreEqual(returnedString, "two", "2 should return 'two'");
        }

        [TestMethod]
        public void TestDoubleDigits()
        {
            int num = 26;

            string returnedString = testClass.NumsToWords(num);

            Assert.AreEqual(returnedString, "twenty-six", "26 should return 'twenty-six'");
        }

        [TestMethod]
        public void TestTripleDigits()
        {
            int num = 209;

            string returnedString = testClass.NumsToWords(num);

            Assert.AreEqual(returnedString, "two hundred and nine", "209 should return 'two hundred and nine'");
        }

        [TestMethod]
        public void FourDigitNumber()
        {
            int num = 7111;

            string returnedString = testClass.NumsToWords(num);

            Assert.AreEqual(returnedString, "seven thousand one hundred and eleven", "7111 should return 'seven thousand one hundred and eleven'");
        }

        [TestMethod]
        public void FiveDigitNumber()
        {
            int num = 87654;

            string returnedString = testClass.NumsToWords(num);

            Assert.AreEqual(returnedString, "eighty-seven thousand six hundred and fifty-four", "87654 should return 'eighty-seven thousand six hundred and fifty-four'");
        }

        [TestMethod]
        public void SixDigitNumber()
        {
            int num = 803308;

            string returnedString = testClass.NumsToWords(num);

            Assert.AreEqual(returnedString, "eight hundred and three thousand three hundred and eight", "803308 should return 'eight hundred and three thousand three hundred and eight'");
        }
    }
}
