﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Exercises
{
    public class NumbersToWords
    {
        string[] singlesPlace = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
                                    "eleven", "twelve", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
        string[] tensPlace = { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };



        public string NumsToWords(int number)
        {

            int workingNum = number;

            string wordString = "";

            if (workingNum / 100000 > 0)
            {
                wordString += (singlesPlace[workingNum / 100000]) + " hundred and ";
                workingNum = workingNum % 100000;
                NumsToWords(workingNum);
            }

            if (workingNum / 10000 > 0)
            {
                wordString += (tensPlace[workingNum / 10000]) + "-";
                workingNum = workingNum % 10000;
                NumsToWords(workingNum);
            }

            if (workingNum / 1000 > 0)
            {
                wordString += (singlesPlace[workingNum / 1000]) + " thousand ";
                workingNum = workingNum % 1000;
                NumsToWords(workingNum);
            }

            if (workingNum / 100 > 0)
            {
                wordString += (singlesPlace[workingNum / 100]) + " hundred and ";
                workingNum = workingNum % 100;
                NumsToWords(workingNum);
            }

            if (workingNum > 20 && workingNum / 10 > 0)
            {
                wordString += (tensPlace[workingNum / 10]) + "-";
                workingNum = workingNum % 10;
                NumsToWords(workingNum);
            }

            if (workingNum < 20)
            {
                wordString += singlesPlace[workingNum];
            }

            return wordString;
        }
    }
}
